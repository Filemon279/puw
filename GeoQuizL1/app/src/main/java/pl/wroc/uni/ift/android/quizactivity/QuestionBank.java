package pl.wroc.uni.ift.android.quizactivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filemon on 11/27/2017.
 */
public class QuestionBank {
    private static QuestionBank ourInstance = new QuestionBank();
    private List<Question> Questions = new ArrayList<Question>();

    private int currentIndex = -1;

    public static QuestionBank getInstance() {
        return ourInstance;
    }

    private QuestionBank() {
        Questions.add(new Question(R.string.question_stolica_polski, true));
        Questions.add(new Question(R.string.question_stolica_dolnego_slaska, false));
        Questions.add(new Question(R.string.question_sniezka, true));
        Questions.add(new Question(R.string.question_wisla, true));
    }

    public Question getQuestion(int index)
    {
        return Questions.get(index);
    }

    public int size()
    {
        return Questions.size();
    }

    public List<Question> getQuestions()
    {
        return Questions;
    }

}
