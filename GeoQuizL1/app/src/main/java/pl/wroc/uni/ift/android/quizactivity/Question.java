package pl.wroc.uni.ift.android.quizactivity;

import android.os.Build;

/**
 * Created by jpola on 26.07.17.
 */

public class Question {

    private int mTextResId;
    private boolean mAnswerTrue;
    private boolean mAnswered;
    private boolean mPoint;
    private boolean mCheater;

    public Question(int textResId, boolean answerTrue)    {

        mTextResId=textResId;
        mAnswerTrue = answerTrue;
        mAnswered = false;
        mPoint = false;
        mCheater = false;
    }

    public int getTextResId() {
        return mTextResId;
    }

    public void setTextResId(int textResId) {
        mTextResId = textResId;
    }

    public boolean isAnswerTrue() {
        return mAnswerTrue;
    }

    public void setAnswered(boolean point)
    {
        mAnswered = true;
        mPoint = point;
    }

    public boolean checkPoint()
    {
        return mPoint;
    }

    public void setCheat()
    {
        mCheater = true;
    }

    public boolean getCheat()
    {
        return mCheater;
    }


    public boolean getAnswered()
    {
        return mAnswered;
    }

    public void setAnswerTrue(boolean answerTrue) {
        mAnswerTrue = answerTrue;
    }
}
