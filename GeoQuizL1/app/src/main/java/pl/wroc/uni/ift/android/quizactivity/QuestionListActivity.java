package pl.wroc.uni.ift.android.quizactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public class QuestionListActivity extends AppCompatActivity {
    private QuestionBank mQuestionsBank = QuestionBank.getInstance();
    private RecyclerView recView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_list);

        recView = (RecyclerView) findViewById(R.id.recView);
        List<Question> Questions = mQuestionsBank.getQuestions();

        recView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recView.setLayoutManager(layoutManager);
        mAdapter = new MyAdapter(Questions);
        recView.setAdapter(mAdapter);
    }
}
